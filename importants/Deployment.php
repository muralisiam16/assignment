<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deployment extends Model
{
    use HasFactory;

    protected $table='deployments';
    protected $fillable=([
        'environment_id',
        'commit_hash'
    ]);
    public $timestamps='false';
}
