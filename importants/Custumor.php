<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Custumor extends Model
{
    use HasFactory;
    protected $table='custumors';
    protected $primary='Customur_id';
    protected $fillable=([
        'Customur_id',
        'name',
        'location'
    ]);
    public function price()
    {
        return $this->hasManyThrough(Price::class,Product::class);
    }
}
