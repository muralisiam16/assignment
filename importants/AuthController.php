<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendMail;


class AuthController extends Controller
{
    public $otp;
    public function requestOtp(Request $request)
 {
        $otp = rand(1000,9999);
        Log::info("otp = ".$otp);
        $user = User::where('email','=',$request->email)->update(['otp' => $otp]); //update otp in user

        if($user){
        // send otp in the email
        $mail_details = [
            'subject' => 'Testing Application OTP Verification',
            'body' => 'Your OTP is : '. $otp
        ];
       
         Mail::to($request->email)->send(new sendmail($mail_details));
       
         return response(["status" => 200, "message" => "OTP sent successfully"]);
        }
        else{
            return response(["status" => 401, 'message' => 'Invalid Request']);
        }
    }
    public function verifyOtp(Request $request){
    
        $user  = User::where([['email','=',$request->email],['otp','=',$request->otp]])->first();
        if($user){
            auth()->login($user, true);
            User::where('email','=',$request->email)->update(['otp' => null]);
            $accessToken = auth()->user()->createToken('authToken')->accessToken;
            return response(["status" => 200, "message" => "Login Successfully", 'user' => auth()->user(), 'access_token' => $accessToken]);
        }
        else{
            return response(["status" => 401, 'message' => 'Invalid otp']);
        }
    }
} 

 /*if(!$admins->password==$request->password and $admins2->user_name ==$request->user_name)
   
   {
    echo "INVALID";
    
    
   }
   else
   {
    echo"LOGIN SUCESSFULLY";
    return otpverify::all(); 
       $sucess=('user_name' == $request->user_name) and ('password' == $request->password);
   }*/