<?php

namespace App\Http\Controllers;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Throwable;

class StudentController extends Controller
{

public function create(Request $request)
{
try{

$request->validate([
    'name'=>'required|string',
    'department'=>'required',
    'location'=>'required',
    'email'=>'required|email',
    'mobile_number'=>'required'
]);
$sutdent=new Student();
$sutdent->name=$request->name;
$sutdent->department=$request->department;
$sutdent->location=$request->location;
$sutdent->email=$request->email;
$sutdent->mobile_number=$request->mobile_number;
$data= view('student',['student'=>[$sutdent]]);
$sutdent->save();
$datas["email"] = "muralimuralidharan65@gmail.com";
$datas["title"] = "STUDENTS DETAILS ";
$datas["body"] = $data;
$pdf = PDF::loadView('newpdf',$datas);
Mail::send('newpdf', $datas, function($message)use($datas, $pdf,$request) {
    $message->to($datas["email"], $datas["email"])
            ->subject($datas["title"])
            ->attachData($pdf->output(), "student.pdf");
});
}
catch (Throwable $e)
{
    return 'data not inserted';
}
return response(["message" => "maill sent successfully"],200);
}

public function show($id)
{
    try{
    $sutdent=Student::find($id);
    return response()->json(['student'=>$sutdent],200);
    }
    catch(ModelNotFoundException $e)
    {
        return $e->getMessage();
    }
}
public function update(Request $request,$id)
{
    try{
    $sutdent=Student::find($id);
    $sutdent->update($request->all());
    echo "Student data updated";
    return $sutdent;
    }
    catch(QueryException $e){
        return $e->getMessage();
    }
}
public function delete($id)
{
    try{
    $sutdent=Student::destroy($id);
    return response()->json(['message'=>'Student Data delete sucessfully']);
    }
    catch(Exception $e)
    {
        return'invalid id';
    }
}
}