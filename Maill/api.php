<?php

use App\Http\Controllers\newController;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register',[PassportAuthController::class,'register']);
Route::post('/login',[PassportAuthController::class,'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    
    return $request->user();
});

Route::post('insert_data',[StudentController::class,'create']);
Route::get('get_data/{id}',[StudentController::class,'show']);
Route::put('update_data/{id}',[StudentController::class,'update']);
Route::delete('delete_data/{id}',[StudentController::class,'delete']);
/*Route::post('/post',[ProductController::class,'create']);
Route::get('/get/{id}',[ProductController::class,'show']);
Route::put('/new/{id}',[ProductController::class,'update']);
Route::delete('/delete/{id}',[ProductController::class,'delete']);
//Route::apiResource('apitest',newController::class);
//Route::resource('/apitest',newController::class);*/
