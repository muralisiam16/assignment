<?php

use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('get_employees_details',[TaskController::class,'show']);
Route::get('get_employee_status',[TaskController::class,'fetch']);
Route::get('popular_email_domain',[TaskController::class,'popular_email_domain']);
Route::get('convert_data',[TaskController::class,'arrays']);
Route::get('highly_ranked_department',[TaskController::class,'highly_ranked_department']);
Route::get('salary_basedon_gender',[TaskController::class,'salary_basedon_gender']);