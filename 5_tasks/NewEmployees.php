<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewEmployees extends Model
{
    use HasFactory;
    public $table='new_employees';
    protected $fillable=([
        'id',
        'FirstName',
        'LastName',
        'Email',
        'Gender',
        'Mobile',
        'Address',
        'City',
        'Designation',
        'Department',
        'status'
    ]);
    public function salaryDetails()
    {
        $this->hasManyThrough(SalaryDetails::class,Department::class);
    }
}
