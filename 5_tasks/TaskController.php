<?php

namespace App\Http\Controllers;

use App\Models\NewEmployees;
use App\Models\SalaryDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use League\Flysystem\SafeStorage;

class TaskController extends Controller
{
    
    public function show()
    {
    
        $newemployee=DB::table('new_employees')
        ->join('departments','new_employees.Department' ,'=', 'departments.id')
        ->join('salary_details', 'salary_details.EmployeeId','=', 'new_employees.id')
        ->select('new_employees.FirstName','new_employees.LastName','new_employees.Email','new_employees.Gender','new_employees.Mobile','new_employees.Address','new_employees.City','new_employees.Designation','new_employees.status AS employeeStatus' ,'departments.Name', 'departments.status AS departmentStatus' ,'salary_details.salary')->get();
        return $newemployee;
// $querry="SELECT new_employees.FirstName,new_employees.LastName,new_employees.Email,new_employees.Gender,new_employees.Mobile,new_employees.Address,new_employees.City,new_employees.Designation,new_employees.status AS employeeStatus ,departments.Name, departments.status AS departmentStatus ,salary_details.salary FROM new_employees INNER JOIN departments ON departments.id = new_employees.Department INNER JOIN salary_details ON salary_details.EmployeeId = new_employees.id";
      
    }
public function fetch()
{
    $newemployee=DB::table('new_employees')
    ->join('departments','new_employees.Department','=','departments.id')
    ->select('new_employees.FirstName','new_employees.LastName','new_employees.Email','new_employees.Gender','new_employees.Mobile','new_employees.Address','new_employees.City','new_employees.Designation','new_employees.status','departments.Name', 'departments.status')->where('new_employees.status','=','active')->where('departments.status','=','active')->groupBy('new_employees.id')->get();
    return$newemployee;

}


public function arrays()
{
    $newemployee=DB::table('new_employees')
    ->join('departments','new_employees.Department' ,'=', 'departments.id')
    ->join('salary_details', 'salary_details.EmployeeId','=', 'new_employees.id')
->select( DB::raw("CONCAT(new_employees.FirstName,' ',new_employees.LastName) AS Name"),DB::raw("CONCAT(new_employees.Designation,'',departments.Name) AS Designation")
,DB::raw("CONCAT(salary_details.salary) AS Salary")
,DB::raw("CONCAT(new_employees.Address,'',new_employees.City,'',new_employees.Email,'',new_employees.Mobile) AS Communication"))->get();
return json_decode($newemployee);
}
public function  popular_email_domain()
{

$newemployee=DB::table('new_employees')
->select(DB::raw('SUBSTRING_INDEX(Email,\'@\',\'-1\') as Emails ,count(*) as total_emails'))
->groupBy('Emails')
    ->orderBy('total_emails',"desc")->first();
    return $newemployee;

}
public function highly_ranked_department()
{
   $newemployee=DB::table('new_employees')
   ->join('departments','new_employees.Department','=', 'departments.id')
   ->join('salary_details','salary_details.EmployeeId','=','new_employees.id')
   ->select(max(['salary_details.salary AS salary']))
   ->select('departments.Name AS DepartmentName','salary_details.salary')
   ->groupBy('new_employees.Department')
   ->orderBy('salary_details.salary','desc')->first();  
        return $newemployee;
}
public function salary_basedon_gender()
{

    $newemployee=DB::table('new_employees')
    ->join('salary_details','salary_details.EmployeeId','=','new_employees.id')
    ->select('new_employees.Gender')
    ->selectRaw('sum(salary_details.salary) as Sum_Of_Salary')
    ->groupBy('new_employees.Gender')->get();
    return $newemployee;
}
}
