<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class otpverify extends Model
{
    use HasFactory;
    public $table='otpverifies';
    protected $fillable=([
        'mobile_no',
        'otp',
        'status'
    ]);
    protected $hidden = [
        'otp'
    ];
}
