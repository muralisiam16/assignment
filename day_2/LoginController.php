<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\otpverify;
use GrahamCampbell\ResultType\Success;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Random;
use Symfony\Component\Console\Input\Input;

class LoginController extends Controller
{
    public function send_otp(Request $request)
    {
        $request->validate(['mobile_no'=>'required|numeric']);
        $otpverify=new otpverify();
        $otpverify->mobile_no=$request->mobile_no;
        $otpverify->save();
        DB::table('otpverifies')->update(['otp'=>'9015']);
        return response()->json(['message'=>'otp sent sucessfully'],200);
    }

    public function verify_otp(Request $request)
    {

     $request->validate([
     'mobile_no'=>'required|numeric',
     'otp'=>'required|numeric'
     ]);
     if($request->otp=='9015')
{
    DB::table('otpverifies')->update(['status'=>'active']);
    return response()->json(['message'=>'User login sucessfully'],200);
}   
else
{
    return response()->json(['message'=>'invalid otp'],404);
}
}
public function admin_login(Request $request)
{

    $request->validate([
        'user_name'=>'required',
        'password'=>'required|string'
    ]);

   
    
$admins=Admin::where('password',$request->password)->first();
$admins2=Admin::where('user_name',$request->user_name)->first();
  if($admins ==null)
{
  return response()->json(['message'=>'invalid password'],404);
}
  if($admins2 == null)
  {
    return response()->json(['message'=>'invalid user_name'],404);
  }
  else
  {
   echo"LOGIN SUCESSFULLY";
   return otpverify::all(); 
  }
    }
    public function forget_password(Request $request)
{
    
  $admins=Admin::where('user_name',$request->user_name)->first(); 
    if($admins==null)
    {
        echo "invalid user name";
    }
    else
    {
        $admins =Admin::where('user_name','=',$request->user_name)->update(['password'=> $request->password]); 
        return response()->json(['message'=>'password changed Sucessfully'],200);
    }

    }
}


