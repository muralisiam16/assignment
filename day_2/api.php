<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MobileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::any('/request_otp',[AuthController::class,'requestOtp']);
Route::post('/verify_otp',[AuthController::class,'verifyOtp']);
Route::get('show',[HomeController::class,'show'])->name('home');
Route::get('send',[MobileController::class,'index']);
Route::post('send_post',[MobileController::class,'send']);


Route::post('user/sent_otp',[LoginController::class,'send_otp']);
Route::post('user/verify_otp',[LoginController::class,'verify_otp']);
Route::post('admin/admin_login',[LoginController::class,'admin_login']);
Route::post('admin/forget_password',[LoginController::class,'forget_password']);