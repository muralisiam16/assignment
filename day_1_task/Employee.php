<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table='employees';
    protected $fillable=([
        'EMP_NO',
        'EMP_NAME',
        'DEPARTMENT',
        'date',
        'DAY_AMOUNT'
        

    ]);
}
