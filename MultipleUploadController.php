<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class MultipleUploadController extends Controller
{
    public function store(Request $request)
    {
if(!$request->hasFile('fileName')){
    return response()->json(['upload file is not found'],400);
}
$allowedfileExtension=['pdf','jpg','png'];
$files = $request->file('fileName'); 
$errors= [];
foreach($files as $file)
{
    
    $extension = $file->getClientOriginalExtension();
    $check= in_array($extension ,$allowedfileExtension);
    if($check)
    {
        foreach($request->fileName  as $mediaFiles){
          $path= $mediaFiles->store('public/images');
          $name=$mediaFiles->getClientOriginalExtension();
          
          //store image files in to the database
          $save= new Image();
          $save->title=$name;
          $save->path=$path;
          $save->save();
        }
    }
    else{
        return response()->json(['invalid_file_format'],422);
    }
    return response()->json(['file uploaded'],200);
}
    }


    public function show(Request $request)
    {

    if(!$request->hasFile('fileName')) {
        return response()->json(['upload_file_not_found'], 400);
    }
    $allowedfileExtension=['pdf','jpg','png'];
    $files = $request->file('fileName'); 
 
    foreach ($files as $file) {      
 
        $extension = $file->getClientOriginalExtension();
 
        $check = in_array($extension,$allowedfileExtension);
 
        if($check) {
            foreach($request->fileName as $mediaFiles) {
 
                $path = $mediaFiles->move(public_path());
                $name = $mediaFiles->getClientOriginalName();
                $save = new Image();
                $save->title = $name;
                $save->path = $path;
                $save->save();
            }
         } 
        else {
            return response()->json(['invalid_file_format'], 422);
        }
 
        return response()->json(['file_uploaded'], 200);
 
    }
}

public function upload(Request $request) 
{ 
    $validator = Validator::make($request->all(),[ 
          'file' => 'required|mimes:doc,docx,pdf,txt,csv,jpg,jpeg|max:2048',
    ]);   

    if($validator->fails()) {          
         
        return response()->json(['error'=>$validator->errors()], 401);                        
     }  


    if ($file = $request->file('file')) {
        $names = time().'.'.$file->extension();
        $path = $file->move(public_path(),$names);
        $name = $file->getClientOriginalName();

        //store your file into directory and db
        $save = new Image();
        $save->title = $file;
        $save->path= $path;
        $save->save();
           
        return response()->json([
            "success" => true,
            "message" => "File successfully uploaded",
            "file" => $file
        ]);

    }


}
}